<?php

use Illuminate\Support\Facades\Route;
use \App\Models\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/test', function () {
    return 'complete !';
});

//$users = User::where('id', '>', 0)->delete();
//dump($users);



// Главная
//Route::get('/', function () {
//    return view('content_shop.contents.home');
//});
//
////Категории
//
//Route::get('/categories', function () {
//    return view('content_shop.contents.categories');
//});
//
////Категория
//Route::get('/category/{id}', function () {
//    return view('content_shop.contents.category');
//});
//
////Продукт/группа
//Route::group(['prefix' => '/product/{id}'], function () {
//
//    //Продукт
//    Route::get('/', function () {
//        return view();
//    });
//
//    //Продукт/описание
//    Route::get('/description', function () {
//    return 'description';
//    });
//
//    //Продукт/добавить товар в корзину
//    Route::post('/add-to-cart', function () {
//        return 'add-to-cart';
//    });
//});
//
////Корзина/группа
//Route::group(['prefix' => '/cart'], function () {
//
//    //Корзина
//    Route::get('/', function () {
//        return 'main';
//    });
//
//    //Корзина/оформить заказ
//    Route::post('/checkout', function () {
//        return 'checkout';
//    });
//
//    //Корзина/удалить товар в корзине
//    Route::post('delete-item', function () {
//        return 'delete-item';
//    });
//
////Корзина/инкремент
//    Route::post('/increment', function () {
//        return 'increment';
//    });
//
////Корзина/декремент
//    Route::post('/decrement', function () {
//        return 'decrement';
//    });
//});
//
////Контакты
//Route::get('/Contacts', function () {
//    return view();
//});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
